worker_processes 2
user 'deployer', 'staff'
preload_app true
timeout 30

project_name = 'simple_deployment'

working_directory "/home/deployer/projects/#{ project_name }/current"

listen "/tmp/#{ project_name }.socket", :backlog => 64
pid "/home/deployer/projects/#{ project_name }/shared/pids/unicorn.pid"

# logs
stderr_path "/home/deployer/projects/#{ project_name }/shared/log/unicorn.stderr.log"
stdout_path "/home/deployer/projects/#{ project_name }/shared/log/unicorn.stdout.log"
