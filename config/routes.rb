SimpleDeployment::Application.routes.draw do
  root :to => 'items#index'
  resources :items, :except => [:create, :destroy, :update]
end
